<?php

namespace Onyxia\Component;

use Symfony\Component\Yaml\Yaml;

class Base
{
    const DEFAULT_ERROR_MESSAGE = "The application have encountered a fatal error";
    protected $parameters;

    public function __construct()
    {
        $path_parameters = ROOT_PATH . '/config/parameters.yml';
        if (!file_exists($path_parameters)) {
            $this->displayError('parameters.yml not found');
        }
        $this->parameters = Yaml::parseFile($path_parameters);
        if (empty($this->parameters)) {
            $this->displayError('parameters.yml is empty');
        }
    }

    public function displayError($message = null){
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        $error = $message ?: self::DEFAULT_ERROR_MESSAGE;
        die('Error : ' . $error);
    }
}