<?php

namespace Onyxia\Component;

use Onyxia\Component\Base;
use Symfony\Component\Yaml\Yaml;

class Language extends Base
{
    protected $language;
    protected $parse_apply;
    protected $parse_default;

    public function __construct()
    {
        parent::__construct();
        if (empty($this->parameters['language'])) {
            parent::displayError('language parameter not found');
        }
        $this->language = $this->parameters['language'];
        $language_path_apply = ROOT_PATH . '/languages/' . $this->language . '.yml';
        if(file_exists($language_path_apply))
        {
            $this->parse_apply = Yaml::parseFile($language_path_apply);
        }
        $language_path_default = __DIR__ . '/languages/' . $this->language . '.yml';
        $this->parse_default = Yaml::parseFile($language_path_default);
    }

    public function getTranslationFor($string)
    {
        if (!empty($this->parse_apply) && isset($this->parse_apply[$string])) {
            return $this->parse_apply[$string];
        }
        elseif(isset($this->parse_default[$string])){
            return $this->parse_default[$string];
        }
        return $string;
    }
}