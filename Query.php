<?php

namespace Onyxia\Component;

use Onyxia\Component\Base;
use Symfony\Component\Yaml\Yaml;

class Query extends Base
{
    protected $link;
    protected $host;
    protected $login;
    protected $pass;
    protected $dbname;

    public function __construct()
    {
        parent::__construct();
        if (empty($this->parameters['host'])) {
            parent::displayError('host parameter not found');
        }
        if (empty($this->parameters['login'])) {
            parent::displayError('login parameter not found');
        }
        if (empty($this->parameters['pass'])) {
            parent::displayError('pass parameter not found');
        }
        if (empty($this->parameters['dbname'])) {
            parent::displayError('dbname parameter not found');
        }
        $this->host = $this->parameters['host'];
        $this->login = $this->parameters['login'];
        $this->pass = $this->parameters['pass'];
        $this->dbname = $this->parameters['dbname'];
        $this->crateLink();
    }

    private function crateLink()
    {
        $this->link = mysqli_connect($this->host, $this->login, $this->pass, $this->dbname) or die (mysqli_connect_error());
    }

    private function myQuery($query)
    {
        $result = mysqli_query($this->link, $query) or die (mysqli_error($this->link));
        return $result;
    }

    public function myFetchAssoc($query)
    {
        $result = $this->myQuery($query) or die (mysqli_error($this->link));
        if (!$result)
            return false;
        $tab_res = mysqli_fetch_assoc($result);
        return $tab_res;
    }

    public function myFetchAllAssoc($query)
    {
        $result = $this->myQuery($query) or die (mysqli_error($this->link));
        if (!$result)
            return false;

        $tab_res = [];

        while ($array = mysqli_fetch_assoc($result))
            $tab_res[] = $array;
        return $tab_res;
    }

    public function userExist($pseudo, $password)
    {
        $query = "SELECT `user_id`, `user_pseudo`, `user_password`, `user_role` FROM `user` WHERE `user_pseudo` = '$pseudo' AND `user_password` = '$password'";
        return $this->myFetchAssoc($query);
    }

    public function getUser($user_id)
    {
        $query = "SELECT `user_id`, `user_pseudo`, `user_password`, `user_role` FROM `user` WHERE `user_id` = '$user_id'";
        return $this->myFetchAssoc($query);
    }

    public function userRegistration($pseudo, $password)
    {
        if ($this->userExist($pseudo, $password)) {
            return false;
        }
        $query = "INSERT INTO `user`(`user_pseudo`, `user_password`) VALUES ('$pseudo', '$password')";
        $result = $this->myQuery($query);
        return $result;
    }


    public function getChannels()
    {
        //todo add case for user_id
        // SELECT * FROM `message` LEFT JOIN channel ON channel.channel_id = message.channel_id LEFT JOIN user ON user.user_id = message.user_id
        $query = "SELECT * FROM `message` LEFT JOIN channel ON channel.channel_id = message.channel_id LEFT JOIN user ON user.user_id = message.user_id";
        $results = $this->myFetchAllAssoc($query);
        $array = [];
        foreach ($results as $result) {
            $object = (object) [
                'message_id' => $result['message_id'],
                'message_content' => $result['message_content'],
                'message_datetime' => $result['message_datetime'],
                'message_author' => $result['message_author'],
                'user_id' => $result['user_id'],
            ];
            if (isset($array[$result['channel_id']])) {
                array_push($array[$result['channel_id']]['message'], $object);
            }
            else {
                $array[$result['channel_id']] = ['name' => $result['channel_name'], 'message' => [$object]];
            }
        }
        // $array = ['room_1' => ['name' => 'General', 'message' => $result]];
        return (object) $array;
    }

    public function addMessage($room_id, $message)
    {
        $user = $this->getUser($message->user_id);
        $user_name = $user['user_pseudo'];
        $query = "INSERT INTO `message`(`channel_id`,`user_id`,`message_content`, `message_datetime`, `message_author`) VALUES ('$room_id','$message->message_author','$message->message_content','$message->message_datetime','$user_name')";
        $this->myQuery($query);
        return true;
    }

    public function getRoomForUser($user_id)
    {
        $query = "SELECT channel.channel_name FROM `channel`, message WHERE message.channel_id = channel.channel_id AND message.user_id = '$user_id'";
        $return = [];
        $result = $this->myFetchAllAssoc($query);
        foreach ($result as $key => $value) {
            $name = reset($value);
            $return[$name] = $name;
        }
        return $return;
    }

    public function getRoomIdForUser($user_id)
    {
        $query = "SELECT channel.channel_id FROM `channel`, message WHERE message.channel_id = channel.channel_id AND message.user_id = '$user_id'";
        $return = [];
        $result = $this->myFetchAllAssoc($query);
        foreach ($result as $key => $value) {
            $name = reset($value);
            $return[$name] = $name;
        }
        return $return;
    }

    public function createRoomForUser($channel_name, $user_id)
    {
        $user = $this->getUser($user_id);
        $user_name = $user['user_pseudo'];

        $channel_id = $this->createOrGetRoom($channel_name);
        $query = "INSERT INTO `message`(`channel_id`, `user_id`, `message_content`, `message_author`) VALUES ('$channel_id','$user_id','Welcome $user_name','Serveur')";
        $this->myQuery($query);
        return true;
    }

    public function createOrGetRoom($channel_name)
    {
        $querySelect = "SELECT `channel_id` FROM `channel` WHERE channel_name = '$channel_name'";
        $result = $this->myFetchAssoc($querySelect);
        if ($result != null) {
            return reset($result);
        }
        else {
            $query = "INSERT INTO `channel`(`channel_name`, `channel_type`) VALUES ('$channel_name','texte')";
            $this->myQuery($query);
            $result = $this->myFetchAssoc($querySelect);
            return reset($result);
        }
    }
}