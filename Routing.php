<?php

namespace Onyxia\Component;

use Onyxia\Component\Base;
use Symfony\Component\Yaml\Yaml;

class routing extends Base
{
    const DEFAULT_404_MESSAGE = "Error 404, page not found";
    protected $routes;

    public function __construct()
    {
        parent::__construct();
        $routing_path = ROOT_PATH . '/config/routing.yml';
        if (!file_exists($routing_path)) {
            parent::displayError('routing.yml not found');
        }
        $this->routes = Yaml::parseFile($routing_path);
    }

    public function getRootAction($action)
    {
        if(isset($this->routes[$action])){
            return $this->routes[$action];
        }
        else
        {
            $this->generate404();
        }
    }

    public function generate404($message = null)
    {
        header("HTTP/1.0 404 Not Found");
        $error = $message ?: self::DEFAULT_404_MESSAGE;
        die($error);
    }

    public function redirect($action)
    {
        header("Location: ?action=" . $action);
        exit;
    }
}